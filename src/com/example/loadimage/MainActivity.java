package com.example.loadimage;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.TimeUnit;

import org.apache.http.util.ByteArrayBuffer;

import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	private Button loadBtn;
	private ProgressBar progress;
	private ImageView image;
	
	private final static String imageURL = "http://static.giantbomb.com/uploads/original/15/157771/2312719-a6.jpg"; 
	private final static String SDCARD_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
	
	final int max = 100;
	private int count;
	
	private ImageThread thread;
	private Thread t;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        progress = (ProgressBar) this.findViewById(R.id.progressBar1);
        progress.setMax(max);
        progress.setProgress(0);
        
        loadBtn = (Button) this.findViewById(R.id.loadBtn);
        loadBtn.setOnClickListener(this);
        
        image = (ImageView) this.findViewById(R.id.imageView1);
        image.setVisibility(View.INVISIBLE);
        
        final Handler h = new Handler() {
        	@Override
        	public void handleMessage(Message msg) {
        		progress.setProgress(msg.what);
        		if(msg.what == max) {
        			Bitmap bmImg = BitmapFactory.decodeFile(SDCARD_PATH + "/image.jpg");
        			image.setImageBitmap(bmImg);
        			image.setVisibility(View.VISIBLE);
        		}
        	}
        };
        
        t = new Thread() {
        	@Override
        	public void run() {
        		int count;
        		try {
        			URL url = new URL(imageURL);
        			URLConnection connection = url.openConnection();
        			connection.connect();
        			// file length
        			int size = connection.getContentLength();
        			
        			InputStream input = new BufferedInputStream(url.openStream(), 8192);
        			OutputStream output = new FileOutputStream(SDCARD_PATH + "/image.jpg");
        			
        			byte data[] = new byte[1024];
        			long total = 0;
        			
        			while((count = input.read(data)) != -1) {
        				total += count;
        				h.sendEmptyMessage((int)(total/size*100));
        				output.write(data, 0, count);
        			}
        			
        			output.flush();        			
        			output.close();
        			input.close();
        		} catch (Exception e) {
        			e.printStackTrace();
        		}
        	}
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case(R.id.loadBtn):
			t.start();
			break;
		}
	}
}